module gitlab.com/frog_in_fog000/comments_ozon

go 1.22.1

require (
	github.com/99designs/gqlgen v0.17.47
	github.com/google/uuid v1.6.0
	github.com/gorilla/websocket v1.5.0
	github.com/ilyakaznacheev/cleanenv v1.5.0
	github.com/jackc/pgx/v5 v5.5.5
	github.com/vektah/gqlparser/v2 v2.5.12
)

require (
	github.com/BurntSushi/toml v1.3.2 // indirect
	github.com/agnivade/levenshtein v1.1.1 // indirect
	github.com/hashicorp/golang-lru/v2 v2.0.7 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20221227161230-091c0ba34f0a // indirect
	github.com/jackc/puddle/v2 v2.2.1 // indirect
	github.com/joho/godotenv v1.5.1 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/mitchellh/mapstructure v1.5.0 // indirect
	github.com/rogpeppe/go-internal v1.12.0 // indirect
	github.com/sosodev/duration v1.3.1 // indirect
	golang.org/x/crypto v0.17.0 // indirect
	golang.org/x/sync v0.7.0 // indirect
	golang.org/x/text v0.15.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
	olympos.io/encoding/edn v0.0.0-20201019073823-d3554ca0b0a3 // indirect
)
