package config

type Config struct {
	Port    string `env:"PORT" envDefault:"8080"`
	Host    string `env:"HOST" env-default:"localhost"`
	Storage string `env:"STORAGE" envDefault:"postgres"`

	PostgresHost     string `env:"POSTGRES_HOST" envDefault:"localhost"`
	PostgresPort     string `env:"POSTGRES_PORT" envDefault:"5432"`
	PostgresUser     string `env:"POSTGRES_USER" envDefault:"postgres"`
	PostgresPassword string `env:"POSTGRES_PASSWORD" envDefault:"postgres"`
	PostgresDBName   string `env:"POSTGRES_DB" envDefault:"postgres"`
}
