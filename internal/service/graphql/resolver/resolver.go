package resolver

import (
	model "gitlab.com/frog_in_fog000/comments_ozon/internal/models"
	"gitlab.com/frog_in_fog000/comments_ozon/internal/storage"
	"sync"
)

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.

// Resolver type
type Resolver struct {
	Store            storage.Storage
	CommentObservers map[string][]chan *model.Comment
	Mu               sync.Mutex
}

// NewResolver is a constructor for Resolver type
func NewResolver(storage storage.Storage) *Resolver {
	return &Resolver{
		Store:            storage,
		CommentObservers: make(map[string][]chan *model.Comment),
	}
}
