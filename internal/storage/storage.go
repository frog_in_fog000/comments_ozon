package storage

import model "gitlab.com/frog_in_fog000/comments_ozon/internal/models"

type Storage interface {
	Posts() ([]*model.Post, error)
	PostByID(id string) (*model.Post, error)
	PostIsEnableComment(id string) (bool, error)
	CreatePost(post *model.Post) error
	CreateComment(comment *model.Comment) error
	CommentsByPostID(postID string, limit *int, offset *int) ([]*model.Comment, error)
}
