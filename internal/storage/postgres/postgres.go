package postgres

import (
	"context"
	"errors"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
	"gitlab.com/frog_in_fog000/comments_ozon/internal/models"
	"gitlab.com/frog_in_fog000/comments_ozon/internal/storage"
	"log"
	"time"
)

type postgresStorage struct {
	db *pgxpool.Pool
}

func NewPostgresStorage(connString string) storage.Storage {
	db, err := pgxpool.New(context.Background(), connString)
	if err != nil {
		log.Fatal(err)

	}

	return &postgresStorage{db: db}
}

// CreatePost creates new post
func (s *postgresStorage) CreatePost(post *models.Post) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	_, err := s.db.Exec(ctx,
		`INSERT INTO posts (id, title, content, user_id, allow_comments) VALUES ($1, $2, $3, $4, $5) RETURNING id`,
		post.ID, post.Title, post.Content, post.UserID, post.AllowComments)
	if err != nil {
		return err
	}

	return nil
}

// CreateComment creates new comment
func (s *postgresStorage) CreateComment(comment *models.Comment) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	_, err := s.db.Exec(ctx,
		`INSERT INTO comments (id, post_id, user_id, content, parent_id) VALUES ($1, $2, $3, $4, $5) RETURNING id`,
		comment.ID, comment.PostID, comment.UserID, comment.Content, comment.ParentCommentID,
	)
	if err != nil {
		return err
	}

	return nil
}

// Posts returns all posts in storage
func (s *postgresStorage) Posts() ([]*models.Post, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	rows, err := s.db.Query(ctx,
		`SELECT id, title, content, user_id, allow_comments 
				FROM posts`)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return []*models.Post{}, nil
		}
		return nil, err
	}
	defer rows.Close()
	posts := make([]*models.Post, 0)
	for rows.Next() {
		var post models.Post
		if err = rows.Scan(&post.ID, &post.Title, &post.Content, &post.UserID, &post.AllowComments); err != nil {
			return nil, err
		}
		posts = append(posts, &post)
	}

	return posts, nil
}

// PostByID returns post by ID with all it's comments
func (s *postgresStorage) PostByID(id string) (*models.Post, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	var post models.Post

	rows, err := s.db.Query(ctx,
		`
		SELECT 
			p.id, p.title, p.content, p.user_id, p.allow_comments,
			c.id, c.post_id, c.user_id, c.content, c.parent_id
		FROM 
			posts p
		LEFT JOIN 
			comments c 
		ON 
			p.id = c.post_id
		WHERE 
			p.id = $1;
	`, id)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return &models.Post{}, nil
		}
		return nil, err
	}
	defer rows.Close()

	var comments []*models.Comment

	for rows.Next() {
		var comment models.Comment

		if err = rows.Scan(&post.ID, &post.Title, &post.Content, &post.UserID, &post.AllowComments,
			&comment.ID, &comment.PostID, &comment.UserID, &comment.Content, &comment.ParentCommentID); err != nil {
			return nil, err
		}

		comments = append(comments, &comment)
	}

	post.Comments = comments

	return &post, nil
}

// CommentsByPostID returns comments of post by ID using pagination
func (s *postgresStorage) CommentsByPostID(postID string, limit *int, offset *int) ([]*models.Comment, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	rows, err := s.db.Query(ctx,
		`SELECT id, post_id, user_id, content, parent_id
				FROM comments
					WHERE post_id = $1
						LIMIT $2
							OFFSET $3`, postID, limit, offset)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return []*models.Comment{}, nil
		}
		return nil, err
	}
	defer rows.Close()

	var comments []*models.Comment

	for rows.Next() {
		var comment models.Comment
		if err = rows.Scan(&comment.ID, &comment.PostID, &comment.UserID, &comment.Content, &comment.ParentCommentID); err != nil {
			return nil, err
		}
		comments = append(comments, &comment)
	}

	return comments, nil
}

// PostIsEnableComment checks if post can have comments or not
func (s *postgresStorage) PostIsEnableComment(id string) (bool, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	var result bool

	err := s.db.QueryRow(ctx,
		`SELECT allow_comments
				FROM posts 
					WHERE id = $1`, id).Scan(&result)
	if err != nil {
		return false, errors.New("failed to check if post is enabled")
	}

	return result, nil
}
