package in_memory

import (
	"errors"
	"gitlab.com/frog_in_fog000/comments_ozon/internal/models"
	"gitlab.com/frog_in_fog000/comments_ozon/internal/storage"
	"log"
)

type inMemoryStorage struct {
	posts    []*models.Post
	comments []*models.Comment
}

func NewInMemoryStorage() storage.Storage {
	return &inMemoryStorage{posts: []*models.Post{}, comments: []*models.Comment{}}
}

// CreatePost creates new post
func (s *inMemoryStorage) CreatePost(post *models.Post) error {
	s.posts = append(s.posts, post)
	return nil
}

// CreateComment creates new comment
func (s *inMemoryStorage) CreateComment(comment *models.Comment) error {
	s.comments = append(s.comments, comment)
	return nil
}

// Posts returns all posts in storage
func (s *inMemoryStorage) Posts() ([]*models.Post, error) {
	if len(s.posts) == 0 {
		return []*models.Post{}, errors.New("posts not found")
	}
	return s.posts, nil
}

// PostByID returns post by ID with all it's comments
func (s *inMemoryStorage) PostByID(id string) (*models.Post, error) {
	var post *models.Post
	var comments []*models.Comment
	for _, p := range s.posts {
		if p.ID == id {
			post = p
		}
	}

	if post == nil {
		return &models.Post{}, errors.New("post not found")
	}

	for _, c := range s.comments {
		if c.PostID == id {
			comments = append(comments, c)
		}
	}

	if len(comments) == 0 {
		log.Println("no comments found")
	}

	post.Comments = comments

	return post, nil
}

// CommentsByPostID returns comments of post by ID using pagination
func (s *inMemoryStorage) CommentsByPostID(postID string, limit *int, offset *int) ([]*models.Comment, error) {
	var comments []*models.Comment
	if *limit > len(s.comments) || *limit < 0 {
		return []*models.Comment{}, errors.New("limit exceeded")
	} else if *offset > len(s.comments) || *offset < 0 {
		return []*models.Comment{}, errors.New("offset exceeded")
	}

	for i := *offset; i < *limit; i++ {
		if s.comments[i].PostID == postID {
			comments = append(comments, s.comments[i])
		}
	}
	return comments, nil
}

// PostIsEnableComment checks if post can have comments or not
func (s *inMemoryStorage) PostIsEnableComment(id string) (bool, error) {
	var post *models.Post
	for _, currentPost := range s.posts {
		if currentPost.ID == id {
			post = currentPost
		}
	}

	if post == nil {
		return false, errors.New("post not found")
	}

	return post.AllowComments, nil
}
