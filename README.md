# Сервис по работе с комментариями
Запуск:
1. С использованием docker-compose:
```bash
docker-compose up -d
```

2. С использованием Makefile:
```bash
make up_build
```

Для тестирования сервиса необходимо перейти:
```url
http://localhost:8080
```

## Примеры работы

### Создание поста

Запрос:
```graphql
mutation {
  createPost(title: "First Post", content: "This is the content of the first post", userId: "user1", allowComments:true) {
    id
    title
    content
    userId
    allowComments
  }
}
```
Ответ:
```json
{
  "data": {
    "createPost": {
      "id": "15ecf7e5-ef5d-481d-b3e1-0d21e05b4a11",
      "title": "First Post",
      "content": "This is the content of the first post",
      "userId": "user1",
      "allowComments": true
    }
  }
}
```

### Создание комментария

Запрос:
```graphql
mutation {
  createComment(postId: "15ecf7e5-ef5d-481d-b3e1-0d21e05b4a11", userId: "user1", content: "This is a comment on the first post", parentId: null) {
    id
    postId
    userId
    content
    parentCommentId
  }
}
```

Ответ:

```json
{
  "data": {
    "createComment": {
      "id": "ba41d4cd-b017-4a02-81cf-abd60367cf92",
      "postId": "15ecf7e5-ef5d-481d-b3e1-0d21e05b4a11",
      "userId": "user1",
      "content": "This is a comment on the first post",
      "parentCommentId": null
    }
  }
}
```

Создание вложенного комментария

```graphql
mutation {
  createComment(postId: "15ecf7e5-ef5d-481d-b3e1-0d21e05b4a11", userId: "user1", content: "This is a comment on the first post", parentId: "ba41d4cd-b017-4a02-81cf-abd60367cf92") {
    id
    postId
    userId
    content
    parentCommentId
  }
}
```

Ответ:

```json
{
  "data": {
    "createComment": {
      "id": "c08c913a-f2ee-4cff-8328-77ec92a229fe",
      "postId": "15ecf7e5-ef5d-481d-b3e1-0d21e05b4a11",
      "userId": "user1",
      "content": "This is a comment on the first post",
      "parentCommentId": "ba41d4cd-b017-4a02-81cf-abd60367cf92"
    }
  }
}
```

### Получение списка всех постов

Запрос:

```graphql
query {
  posts {
    id
    title
    content
    userId
    allowComments
  }
}

```

Ответ:

```json
{
  "data": {
    "posts": [
      {
        "id": "15ecf7e5-ef5d-481d-b3e1-0d21e05b4a11",
        "title": "First Post",
        "content": "This is the content of the first post",
        "userId": "user1",
        "allowComments": true
      }
    ]
  }
}
```

### Получение поста со всеми комментариями по ID

Запрос:

```graphql
query {
  post(id:"15ecf7e5-ef5d-481d-b3e1-0d21e05b4a11") {
    id
    title
    content
    userId
    allowComments
    comments{
      id
      postId
      userId
      content
      parentCommentId
    }
  }
}

```

Ответ:

```json
{
  "data": {
    "post": {
      "id": "15ecf7e5-ef5d-481d-b3e1-0d21e05b4a11",
      "title": "First Post",
      "content": "This is the content of the first post",
      "userId": "user1",
      "allowComments": true,
      "comments": [
        {
          "id": "ba41d4cd-b017-4a02-81cf-abd60367cf92",
          "postId": "15ecf7e5-ef5d-481d-b3e1-0d21e05b4a11",
          "userId": "user1",
          "content": "This is a comment on the first post",
          "parentCommentId": null
        },
        {
          "id": "c08c913a-f2ee-4cff-8328-77ec92a229fe",
          "postId": "15ecf7e5-ef5d-481d-b3e1-0d21e05b4a11",
          "userId": "user1",
          "content": "This is a comment on the first post",
          "parentCommentId": "ba41d4cd-b017-4a02-81cf-abd60367cf92"
        }
      ]
    }
  }
}
```

### Получение списка комментариев у поста по его ID (с пагинацией)

Запрос:

```graphql
query {
  comments(post_id:"15ecf7e5-ef5d-481d-b3e1-0d21e05b4a11", 
  				limit:2, offset:0) 
  {
    id
    postId
    userId
    content
    parentCommentId
  }
}

```

Ответ:

```json
{
  "data": {
    "comments": [
      {
        "id": "ba41d4cd-b017-4a02-81cf-abd60367cf92",
        "postId": "15ecf7e5-ef5d-481d-b3e1-0d21e05b4a11",
        "userId": "user1",
        "content": "This is a comment on the first post",
        "parentCommentId": null
      },
      {
        "id": "c08c913a-f2ee-4cff-8328-77ec92a229fe",
        "postId": "15ecf7e5-ef5d-481d-b3e1-0d21e05b4a11",
        "userId": "user1",
        "content": "This is a comment on the first post",
        "parentCommentId": "ba41d4cd-b017-4a02-81cf-abd60367cf92"
      }
    ]
  }
}
```

### Подписка на канал с комментариями к посту по его ID

Запрос:

```graphql
subscription {
  commentAdded(postId: "15ecf7e5-ef5d-481d-b3e1-0d21e05b4a11") {
    id
    postId
    userId
    content
    parentCommentId
  }
}
```

Ответ:

При добавления комментариев они отображаются без повторной отправки запросов

```json
{
  "data": {
    "commentAdded": {
      "id": "7146fc21-1e12-43f1-9b89-fea3df2e080a",
      "postId": "15ecf7e5-ef5d-481d-b3e1-0d21e05b4a11",
      "userId": "user1",
      "content": "This is a comment on the first post EMBEDED",
      "parentCommentId": null
    }
  }
}
```

