package main

import (
	"fmt"
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/handler/transport"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/gorilla/websocket"
	"github.com/ilyakaznacheev/cleanenv"
	"gitlab.com/frog_in_fog000/comments_ozon/internal/config"
	"gitlab.com/frog_in_fog000/comments_ozon/internal/generated"
	"gitlab.com/frog_in_fog000/comments_ozon/internal/service/graphql/resolver"
	"gitlab.com/frog_in_fog000/comments_ozon/internal/storage"
	"gitlab.com/frog_in_fog000/comments_ozon/internal/storage/in_memory"
	"gitlab.com/frog_in_fog000/comments_ozon/internal/storage/postgres"
	"log"
	"net/http"
)

const (
	memoryStorage     = "memory"
	postgresqlStorage = "postgres"
)

var (
	cfg   config.Config
	store storage.Storage
)

func main() {
	// config init
	if err := cleanenv.ReadEnv(&cfg); err != nil {
		log.Fatal(err)
	}

	// storage init
	if cfg.Storage == memoryStorage {
		store = in_memory.NewInMemoryStorage()
	} else if cfg.Storage == postgresqlStorage {
		connString := fmt.Sprintf("postgres://%s:%s@%s:%s/%s",
			cfg.PostgresUser, cfg.PostgresPassword, cfg.PostgresHost, cfg.PostgresPort, cfg.PostgresDBName)
		store = postgres.NewPostgresStorage(connString)
	} else {
		log.Fatal("Unsupported storage type: " + cfg.Storage)
	}

	// service init
	r := resolver.NewResolver(store)

	// "handlers"
	server := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: r}))
	server.AddTransport(&transport.Websocket{
		Upgrader: websocket.Upgrader{
			CheckOrigin: func(r *http.Request) bool {
				return true
			},
		},
	})

	// routes
	http.Handle("/", playground.Handler("GraphQL playground", "/gql"))
	http.Handle("/gql", server)

	// starting http server
	log.Printf("Service is up and running on port: %s with %s storage", cfg.Port, cfg.Storage)
	log.Fatal(http.ListenAndServe(":"+cfg.Port, nil))
}
